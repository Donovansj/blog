---
title: Week 1
subtitle: Maandag 04-09-2017 t/m vrijdag 08-09-2017
date: 2017-09-04
tags: ["example", "markdown"]
---

### Maandag 
Vandaag was de kick-off van ons eerste project. We kregen een korte briefing en konden kennismaken met onze groepjes. Als groepje hebben we daarna de opdracht doorgelezen en een brainstorm sessie gehouden over mogelijke ideeën. Helaas bleven we een beetje hangen met een niet al te inspirerend idee, dat eerlijk gezegd ook ons niet leuk leek om te spelen. Het was namelijk een standaard startweek spel dat i.p.v. met HR-medewerkers via de app werd gespeeld.

Opvallend was dat na de pauze, zonder enige gedachte naar de opdracht toe de ideeën er weer uitstroomde toen we bij elkaar kwamen. Hieruit rolde dan ook ons definitieve idee van Capture the City. 

### Dinsdag
-
### Woensdag
Vandaag gingen we met ons groepje reizen naar de Hogeschool Rotterdam op de Academieplein. We gingen daar naar toe om onderzoek te doen naar ons doelgroep hierbij hadden we ook wat interviewvragen voorbereid voor de vijf bouwkundestudenten die we geregld hadden. 


Observatie interview / verbeterpunten:
- De vragen eerst even door laten nemen zodat ze op video wat natuurlijker overkomen. 
- Interview meer doorvragen, want de vragen leken hierdoor niet zo samenhangend en er vielen ook wat ongemakkelijke momenten tijdens de interview.
- Ten slotte zouden we de geïnterviewden ook een korte samenvatting kunnen geven over ons spel om zo onduidelijkheden te kunnen voorkomen.

### Donderdag
In de ochtend had ik een indivuduele onderzoek gedaan over wat Rotterdam nou Rotterdam maakt. Hierna heb ik de onderzoek ook visueel gemaakt.
(../img/Moodboard iteratie 1.jpg)


### Vrijdag
Gister kwamen we er achter dat onze eerste interviews toch wat misten, daarom hadden we afgesproken om wat extra mensen te gaan interviewen. In de avond kon ik via skype gelukkig 2 vrienden van mij interviewen over hun startweek bij bouwkunde. Dit had ik toen uitgetypt en het viel me al snel op dat je hierdoor meer diepte weet te krijgen in je antwoorden dan als je dit met een meerkeuzevragen onderzoek zou doen.